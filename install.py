#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
install.py
    Install or link the plugins to GIMP user directory.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""

import os
from subprocess import run, PIPE
from re import search
from argparse import ArgumentParser
from shutil import copytree, rmtree
from json import load

__author__ = 'Sergio Jiménez Herena'
__copyright__ = 'Copyright 2017, Sergio Jiménez Herena'
__license__ = 'GPL3'

HOME_PATH = os.path.expanduser("~")
REPO_PATH = os.path.realpath(".")
P_SRC_DIR = os.path.join(REPO_PATH, 'src_python')
C_SRC_DIR = os.path.join(REPO_PATH, 'src_c')


def get_gimp_version():

    gimp_process = run(['gimp', '-vv'], stdout=PIPE, encoding='utf8')
    match = search('(?:\A.*\s)(\d\.\d+)', gimp_process.stdout)
    return match.group(1)


GIMP_PATH = os.path.join(HOME_PATH, '.config', 'GIMP', get_gimp_version())


def get_plugins():

    plugin_info = load(open('plugins.json'))
    plugins     = {}

    for lang in plugin_info:
        for plugin_id in plugin_info[lang]:
            src_dir = os.path.join(
                REPO_PATH,
                'src_{}'.format(lang),
                plugin_info[lang][plugin_id]['sources']['s_dir']
            )
            plugin = {
                'lang': lang,
                'src': [os.path.join(src_dir, file_path)
                        for file_path in
                        plugin_info[lang][plugin_id]['sources']['files']]
            }

            if 'extra' in plugin_info[lang][plugin_id].keys():
                plugin['extra'] = [
                    {'dst': os.path.join(GIMP_PATH,
                                         'plug-ins',
                                         plugin_id,
                                         f['dst_dir'],
                                         f['dst_name']),
                     'src': os.path.join(src_dir, f['src'])}
                    for f in plugin_info[lang][plugin_id]['extra']
                ]

            plugins[plugin_id] = plugin

    return plugins


def manage_plugin(op, plugin_name, plugins):

    if plugin_name not in plugins.keys():
        print('WARNING: Could not find \'{}\' source files.'
              '\n       Skipping!'.format(plugin_name))
        return

    target_dir = os.path.join(GIMP_PATH, 'plug-ins', plugin_name)

    if not os.path.exists(target_dir):
        os.mkdir(target_dir)

    plugin_lang = plugins[plugin_name]['lang']

    if plugin_lang == 'c':

        for plugin_path in plugins[plugin_name]['src']:

            target_path, plugin_file = os.path.split(plugin_path)
            plugin_file = os.path.splitext(plugin_file)[0]
            target_path = os.path.join(GIMP_PATH, 'plug-ins', plugin_file)
            target      = os.path.join(target_path, plugin_file)

            if op == 'install':
                run(['gimptool-2.0', '--install', plugin_path],
                    stdout=open(os.devnull, 'wb'))
                print("INFO: Installed '{}'".format(target))

            elif op == 'uninstall':
                if os.path.islink(target):
                    print("ERROR: Trying to uninstall a linked plugin.")
                    return
                if os.path.isdir(target_path) and \
                   os.path.isfile(target):
                    rmtree(target_path)
                    print("INFO: Uninstalled '{}'".format(target))

            elif op == 'unlink' or op == 'link':
                if os.path.islink(target):
                    if os.path.isdir(target_path) and \
                       op == 'unlink':
                        rmtree(target_path)
                    os.remove(target)
                elif op == 'unlink':
                    print("ERROR: Trying to unlink an installed plugin.")
                    return
                if op == 'link':
                    os.chdir(C_SRC_DIR)
                    run(['gimptool-2.0', '--build', plugin_path],
                        stdout=open(os.devnull, 'wb'))
                    os.symlink(os.path.abspath(plugin_name),
                               target)
                    os.chdir(REPO_PATH)
                    print("INFO: New link: {}\n"
                          "\tsrc:  '{}'\n\tlink: '{}'\n".format(plugin_name,
                                                                plugin_path,
                                                                target_path))
                else:
                    print("INFO: Remove link: {}\n"
                          "\tlink: '{}'\n"
                          "\tdir:  '{}'".format(plugin_name,
                                                target,
                                                target_path))

    elif plugin_lang == 'python':

        for plugin_path in plugins[plugin_name]['src']:

            plugin_file = os.path.split(plugin_path)[1]
            target_path = os.path.join(GIMP_PATH, 'plug-ins', plugin_name)
            target      = os.path.join(target_path, plugin_file)

            if op == 'install':

                run(['gimptool-2.0', '--install-bin', plugin_path],
                    stdout=open(os.devnull, 'wb'))
                print("INFO: Installed '{}'".format(target))

            elif op == 'uninstall':

                if os.path.isfile(target_path):
                    run(['gimptool-2.0', '--uninstall-bin', plugin_file],
                        stdout=open(os.devnull, 'wb'))
                    print("INFO: Uninstalled '{}'".format(target))

            elif op == 'unlink' or op == 'link':

                if os.path.isdir(target_path) and op == 'unlink':
                    rmtree(target_path)
                if op == 'link':
                    os.chmod(plugin_path, 0o0766)
                    os.symlink(os.path.abspath(plugin_path),
                               target)
                    print("INFO: New link: {}\n"
                          "\tsrc:  '{}'\n\tlink: '{}'\n".format(plugin_name,
                                                                plugin_path,
                                                                target))
                else:
                    print("INFO: Remove link: {}\n"
                          "\tlink: '{}'\n"
                          "\tdir:  '{}'".format(plugin_name,
                                                target,
                                                target_path))

    if 'extra' in plugins[plugin_name].keys():

        for f in plugins[plugin_name]['extra']:

            src = f['src']
            dst = f['dst']

            cond = [op == 'install',        # 0
                    op == 'link',           # 1
                    op == 'uninstall',      # 2
                    op == 'unlink',         # 3
                    os.path.exists(src),    # 4
                    os.path.exists(dst)]    # 5

            if cond[0] and cond[4]:
                copytree(src, dst)
                print("INFO: Installed:\n"
                      "\tsrc:  '{}'\n\tdest: '{}'\n".format(src, dst))
            elif cond[1] and cond[4]:
                if os.path.islink(dst):
                    os.remove(dst)
                os.symlink(src, dst)
                print("INFO: New Link: {}\n"
                      "\tsrc:  '{}'\n\tdest: '{}'\n".format(plugin_name,
                                                            src, dst))
            elif (cond[2] or cond[3]) and cond[5]:
                if os.path.islink(dst):
                    os.remove(dst)
                    print("INFO: Removed link:\n\t{}\n".format(dst))
                if os.path.isdir(dst):
                    rmtree(dst)
                    print("INFO: Removed dir:\n\t{}\n".format(dst))


def main():

    parser = ArgumentParser(description="gimp-plugins install script.")
    parser.add_argument(choices={'list', 'install', 'uninstall',
                                 'link', 'unlink'},
                        dest='operation', action='store')
    parser.add_argument(dest='plugin_names', nargs='*', default=None)

    args = parser.parse_args()

    plugins     = get_plugins()
    operation   = args.operation
    cmd_plugins = args.plugin_names

    if cmd_plugins:
        for plugin in cmd_plugins:
            manage_plugin(operation, plugin, plugins)
    else:
        if operation == 'list':
            plugins = get_plugins()
            for plugin in plugins.keys():
                print(plugin)
        else:
            for plugin in plugins:
                manage_plugin(operation, plugin, plugins)


if __name__ == '__main__':
    main()

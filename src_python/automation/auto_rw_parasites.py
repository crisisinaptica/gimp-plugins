#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
auto_rw_parasites.py
    by Sinaptica.

    Part of a set of scripts to automate the editing of images with Gimp

    Based on scripts and ideas by Stephen Kiel
    See https://www.gimp.org/tutorials/Automate_Editing_in_GIMP/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

"""

from auto_base import *

__author__    = 'Stephen Kiel, Sergio Jiménez Herena'
__copyright__ = 'Copyright 2018, Sergio Jiménez Herena'
__license__   = 'GPL3'


def auto_display_assigned_parasites(img):
    """Displays the parasites assigned to the image in a gimp message window.

    Arguments:
    image (gimp.Image): The image to act upon.
    """

    prop_read = AutoPropReader()

    flag_prop_name_list = prop_read.get_names()

    msg = '**Flow Control Parasites\n'
    for parasite_name in ('flow', 'current_step', 'next_step'):
        parasite = img.parasite_find(parasite_name)
        if parasite:
            msg = '{}{}:   {}\n'.format(msg, parasite.name, parasite.data)

    msg = '{}\n{}\n'.format(msg, '**Status Flag Parasites')
    for parasite_name in flag_prop_name_list:
        parasite = img.parasite_find(parasite_name)
        if parasite:
            if parasite_name in ('update_flag', 'overwrite_xcf',
                                 'overwrite_file'):
                if bool(int(parasite.data)):
                    parasite_data = 'Yes'
                else:
                    parasite_data = 'No'
            else:
                parasite_data = parasite.data
            msg = '{}{}:   {}\n'.format(msg, parasite.name, parasite_data)

    pdb.gimp_message(msg)


register(
    'python_fu_auto_display_assigned_parasites',
    'Display Parasites',
    'Display Image Parasites in Gimp Message',
    'Stephen Kiel',
    'Stephen Kiel',
    'July 2013',
    '0) Display Assigned Parasites',
    '*',
    [
        (PF_IMAGE, 'image', 'Input Image', None)
    ],
    [],
    auto_display_assigned_parasites,
    menu='<Image>/Automation'
)


def auto_set_parasites(image, update, xcf_over, img_over, color, contrast,
                       do_for_all):
    """Attach image parasites.

     Arguments:
     img (gimp.Image): The image to act upon.
     update (bool): Attach update_flag parasite.
     xcf_over (bool): Attach overwrite_xcf flag parasite.
     img_over (bool): Attach overwrite_img flag parasite.
     color (int): enhance_color_level parasite value.
     contrast (int): enhance_contrast_level parasite value.
     do_for_all (bool): Set parasites in all opened images.
     """

    def set_parasites(img):
        if 'flow' not in img.parasite_list():
            return

        img.attach_new_parasite('update_flag', PARAS_IDX,
                                str(int(update)))
        img.attach_new_parasite('overwrite_xcf', PARAS_IDX,
                                str(int(xcf_over)))
        img.attach_new_parasite('overwrite_file', PARAS_IDX,
                                str(int(img_over)))
        img.attach_new_parasite('enhance_color_level', PARAS_IDX,
                                str(color))
        img.attach_new_parasite('enhance_contrast_level', PARAS_IDX,
                                str(contrast))

        if bool(int(img.parasite_find('overwrite_xcf').data)):
            pdb.gimp_file_save(img,
                               img.active_drawable,
                               img.filename,
                               img.filename)
            pdb.gimp_image_clean_all(img)

    if do_for_all:
        for image_in_editor in gimp.image_list():
            set_parasites(image_in_editor)
    else:
        set_parasites(image)


register(
    'python_fu_auto_set_parasites',
    'Set parasites',
    'Set parasites',
    'Sergio Jiménez Herena',
    'Sergio Jiménez Herena',
    'March 2018',
    '2) Set parasites',
    '*',
    [
        (PF_IMAGE, 'image', 'Input Image', None),
        (PF_BOOL, 'update', 'Update', True),
        (PF_BOOL, 'xcf_over', 'Overwrite XCF', False),
        (PF_BOOL, 'img_over', 'Overwrite IMG', False),
        (PF_SLIDER, 'color', 'Color level', 60.0, (0, 100, 0.5)),
        (PF_SLIDER, 'contrast', 'Contrast level', 75.0, (0, 100, 0.5)),
        (PF_BOOL, 'do_for_all', 'All opened images', False)

    ],
    [],
    auto_set_parasites,
    menu='<Image>/Automation'
)

#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
auto_write_xml.py

    Part of a set of scripts to automate the editing of images with Gimp

    Based on scripts and ideas by Stephen Kiel
    See https://www.gimp.org/tutorials/Automate_Editing_in_GIMP/

    Modifications by Sinaptica.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

"""

from auto_base import *

__author__    = 'Stephen Kiel, Sergio Jiménez Herena'
__copyright__ = 'Copyright 2013, Stephen Kiel'
__license__   = 'GPL3'


def auto_write_xml():
    """Read the pseudo code files and write the combined xml files to disk.

    The methods gen_commander_xml, gen_flow_xml, and gen_property_xml read the
    pseudo code and write xml in the 'commander', 'flow', and 'properties'
    subdirectories of the auto_xml directory.
    The xml written with this command is referenced by the AutoXmlReader
    class which is also in the autoBase module and furnishes information
    to the AutoUpdate and Commander plug-in scripts.
    """

    xml_write_obj = AutoXmlGenerator()
    xml_write_obj.gen_commander()
    xml_write_obj.gen_flow()
    xml_write_obj.gen_property()
    pdb.gimp_message("Wrote / Updated xml for:"
                     "\n\tCommander\n\tProperties\n\tFlow")


register(
    "python_fu_auto_write_xml",
    "Write pseudo code to xml",
    "Write pseudo code to xml",
    "Stephen Kiel, Sergio Jiménez Herena",
    "Stephen Kiel",
    "Aug 2013",
    "Pseudo code to XML",
    "",
    [],
    [],
    auto_write_xml,
    menu="<Image>/Automation/Utilities",
    on_query=auto_write_xml
)

commander>Color Adjust

    macro>

        comment> Merge the work layers together and raise the color layer to the top
        comment>First the Retinex layer
        >>> retinex_layer = pdb.gimp_image_get_layer_by_name(img, 'retinex')
        >>> temp_layer = pdb.gimp_image_merge_down(img, retinex_layer, EXPAND_AS_NECESSARY)

        comment> Then the Sharpen Layer
        >>> sharpen_layer = pdb.gimp_image_get_layer_by_name(img, 'sharpen')
        >>> new_layer = pdb.gimp_image_merge_down(img, sharpen_layer, EXPAND_AS_NECESSARY)
        >>> new_layer.name = 'contrast'

        comment> Desaturate the resultant layer
        >>> pdb.gimp_drawable_desaturate(new_layer, DESATURATE_LIGHTNESS)

        comment> Move the Color Layer to the top of the stack, name it ColorBase, set opacity to 80%
        >>> color_layer = pdb.gimp_image_get_layer_by_name(img, 'color_layer')
        >>> color_layer.name = 'color_base'
        >>> pdb.gimp_image_raise_item_to_top(img, color_layer)
        >>> color_layer.opacity = 80.0

        comment> Copy the Color Layer, call it color_add, set opacity to 20%.  Adjust by hand as needed.
        >>> color_add = pdb.gimp_layer_copy(color_layer, FALSE)
        >>> img.add_layer(color_add, 0)
        >>> color_add.name = 'color_add'
        >>> color_add.opacity = 20.0

        comment> Adjust the Color Layer opacity based on parasite enhance_color_level

        >>> parasites = pdb.gimp_get_parasite_list()[1]
        >>> color_level = None
        >>> if 'enhance_color_level' in parasites: color_level = img.parasite_find('enhance_color_level').data
        >>> if color_level is not None: color_add.opacity = float(color_level)

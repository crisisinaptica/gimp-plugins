#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
pyzealous.py
    Pyzealous GIMP plug-in
    by Sinaptica, based on "crop-zealous.c" by Adam D. Moss


This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""

from __future__ import print_function, division
from gimpfu import *

__author__ = 'Sergio Jiménez Herena'
__copyright__ = '2017, Sergio Jiménez Herena'
__license__ = 'GPL3'


def pyzealous(image, layer, padding):

    image.disable_undo()
    gimp.progress_init()
    pdb.gimp_selection_none(image)

    padding = abs(padding)

    gimp.tile_cache_ntiles(
                           1 +  # Round up
                           4 *  # Shadow tiles
                           (layer.width // gimp.tile_width() if
                            layer.width > layer.height else
                            layer.height // gimp.tile_height())
                           )

    region_read = layer.get_pixel_rgn(0, 0, image.width, image.height,
                                      False, False)

    image_shadow = layer.get_pixel_rgn(0, 0, image.width, image.height,
                                       True, True)

    kill_rows = []
    living_rows = 0

    for row in range(0, image.height):
        row_bytes = bytearray(region_read[0:image.width, row])
        kill_rows.append(True)

        for pixel in range(1, image.width):
            if not colors_equal(0, pixel, row_bytes, layer.bpp):
                kill_rows[row] = False
                living_rows += 1
                break

    gimp.progress_update(0.25)

    kill_cols = []
    living_cols = 0

    for col in range(0, image.width):
        col_bytes = bytearray(region_read[col, 0:image.height])
        kill_cols.append(True)

        for pixel in range(1, image.height):
            if not colors_equal(0, pixel, col_bytes, layer.bpp):
                kill_cols[col] = False
                living_cols += 1
                break

    gimp.progress_update(0.50)

    if (
        (living_cols == 0 or living_rows == 0) or
        (living_cols == image.width and living_rows == image.height)
       ):
        gimp.message("Noting to crop.")
        return

    if padding != 0:
        kill_rows, living_rows = pad_line(kill_rows, living_rows, padding)
        kill_cols, living_cols = pad_line(kill_cols, living_cols, padding)

    destrow = 0
    for row in range(0, image.height):
        if not kill_rows[row]:
            copy = region_read[0:image.width, row]
            image_shadow[0:image.width, destrow] = copy
            destrow += 1

    gimp.progress_update(0.75)

    destcol = 0
    for col in range(0, image.width):
        if not kill_cols[col]:
            copy = image_shadow[col, 0:image.height]
            image_shadow[destcol, 0:image.height] = copy
            destcol += 1

    gimp.progress_update(1.0)

    layer.flush()
    layer.merge_shadow(True)
    layer.update(0, 0, image.width, image.height)

    pdb.gimp_image_crop(image, living_cols, living_rows, 0, 0)
    pdb.plug_in_autocrop(image, layer)
    image.resize(image.width + padding * 2,
                 image.height + padding * 2,
                 padding, padding)

    return


def pad_line(kill_lines, living_lines, padding):

    kill_buffer = list(kill_lines)

    for idx in range(0, len(kill_buffer)):

        curr_line = kill_lines[idx]
        if idx == 0:
            prev_line = curr_line
        else:
            prev_line = kill_lines[idx - 1]

        if idx == len(kill_lines) - 1:
            next_line = curr_line
        else:
            next_line = kill_lines[idx + 1]

        if curr_line and not next_line:
            back = max(0, min(idx - padding, len(kill_lines)))
            for pad_back in range(idx, back, -1):
                if kill_lines[pad_back]:
                    kill_buffer.pop(pad_back)
                    kill_buffer.insert(pad_back, False)
                    living_lines += 1

        if not prev_line and curr_line:
            forward = max(0, min(idx + padding, len(kill_lines)))
            for pad_forward in range(idx, forward):
                if kill_lines[pad_forward]:
                    kill_buffer.pop(pad_forward)
                    kill_buffer.insert(pad_forward, False)
                    living_lines += 1

    return kill_buffer, living_lines


def colors_equal(pix_1, pix_2, line, bpp):

    pix_1 *= bpp
    pix_2 *= bpp

    if bpp is 1:
        color_1 = line[pix_1]
        color_2 = line[pix_2]
    elif bpp is 2:
        color_1 = (line[pix_1], line[pix_1 + 1])
        color_2 = (line[pix_2], line[pix_2 + 1])
    elif bpp is 3:
        color_1 = (line[pix_1], line[pix_1 + 1], line[pix_1 + 2])
        color_2 = (line[pix_2], line[pix_2 + 1], line[pix_2 + 2])
    elif bpp is 4:
        color_1 = line[pix_1 + 3]
        color_2 = line[pix_2 + 3]
    else:
        return False

    if bpp == 4 and color_1 == 0 and color_2 == 0:
        return True
    elif color_1 == color_2:
        return True
    else:
        return False


register(
    "pyzealous",
    "Zealous Crop extended.",
    "Zealous Crop with some extra functionality.",
    "Sergio Jiménez Herena",
    "Sergio Jiménez Herena",
    "2017",
    "pyzealous...",
    "*",
    [
        (PF_IMAGE, "image", "Image", None),
        (PF_DRAWABLE, "drawable", "Drawable", None),
        (PF_INT, "padding", "Padding", 0)
    ],
    [],
    pyzealous,
    menu="<Image>/Image/Crop"
)

main()
